#!/bin/bash

set -e

cat >child-pipeline-gitlab-ci.yml <<EOL
stages:
  - build-apps

build-app-1:
  stage: build-apps
  script:
    - cat app1/app1.txt
  rules:
    - changes:
        - app1/*

build-app-2:
  stage: build-apps
  script:
    - cat app2/app2.txt
  rules:
    - changes:
        - app2/*
EOL
